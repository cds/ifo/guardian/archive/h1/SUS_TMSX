# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

from SUS import *

# Redefine the state while we are using the TMS servos in lock.
# The TMS servos use the test banks, and the normal aligned state
# will give a notification stating such. 
ALIGNED = get_idle_state()
ALIGNED.index = 100

prefix = 'SUS-TMSX'

##################################################
# SVN $Id$
# $HeadURL$
##################################################
